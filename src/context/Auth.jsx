import React, {createContext, useEffect, useState} from 'react';

import { initializeApp } from 'firebase/app';
import { getFirestore} from 'firebase/firestore';
import { getAuth} from "firebase/auth";




const firebaseConfig = {
  apiKey: "AIzaSyAi2IBMGJ_En3_hIRha4wwAVkSu7nPVraY",
authDomain: "brome-5e406.firebaseapp.com",
databaseURL: "https://brome-5e406-default-rtdb.europe-west1.firebasedatabase.app",
projectId: "brome-5e406",
storageBucket: "brome-5e406.appspot.com",
messagingSenderId: "130755062271",
appId: "1:130755062271:web:afdd4c7264793564de5fca",
measurementId: "G-ZBXLWKV8L4"
};


const db = getFirestore(app);
const app = initializeApp(firebaseConfig);




export const AuthContext = createContext()


export function AuthProvider ({children}) {


    const [newCurrentUser, setCurrentUser] = useState();


useEffect(() => {

  const unsub = getAuth().onAuthStateChanged(user => {
    setCurrentUser(user)
 
  })
    return unsub
}, [])

const value = {
  newCurrentUser
}
          
     
            
    return (
        <AuthContext.Provider value={(value)}>
            {children}
        </AuthContext.Provider>
    )

    }