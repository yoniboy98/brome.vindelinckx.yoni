import React from 'react';
import {
  Page,
  Navbar,
  List,
  ListItem,
  Subnavbar,
  Searchbar,
  BlockTitle,
} from 'framework7-react';

import MessageList from '../components/MessageList';





const SettingsPage = () => {


  return(
    <Page name="settings">
    <Navbar title="Messages" />
    <Subnavbar inner={false}>
        <Searchbar
          searchContainer=".search-list2"
          searchIn=".item-title"
     
        ></Searchbar>
      </Subnavbar>



    <BlockTitle>Messages</BlockTitle>
    <List className="searchbar-not-found">
      <ListItem title="Nothing found"></ListItem>
    </List>
  
        
  <MessageList/>
       


  </Page>


  )}






export default SettingsPage;
