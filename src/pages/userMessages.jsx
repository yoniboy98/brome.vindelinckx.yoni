import React, { useState , useContext, useEffect} from 'react';
import { Page, Navbar, List,f7, Button,Messages,MessagesTitle,Message ,NavRight,Link, BlockTitle, ListInput } from 'framework7-react';
import { initializeApp } from 'firebase/app';
import { getFirestore,addDoc, collection, onSnapshot, query, orderBy} from 'firebase/firestore';

import { AuthContext } from '../context/Auth';


const firebaseConfig = {
    apiKey: "AIzaSyAi2IBMGJ_En3_hIRha4wwAVkSu7nPVraY",
  authDomain: "brome-5e406.firebaseapp.com",
  databaseURL: "https://brome-5e406-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "brome-5e406",
  storageBucket: "brome-5e406.appspot.com",
  messagingSenderId: "130755062271",
  appId: "1:130755062271:web:afdd4c7264793564de5fca",
  measurementId: "G-ZBXLWKV8L4"
  };
  
  
  const db = getFirestore(app);
  const app = initializeApp(firebaseConfig);




const UserMessagesPage = (props) => {

console.log(props)

const {newCurrentUser} = useContext(AuthContext)

const [allMessages, setAllMessages] = useState([])

const [messageInput, setMessageInput] = useState("")


 //messages ophalen
 useEffect(() => {
  const getMessages = async () => {
    const q = query(collection(db, "friendships" , props.id, "messages"),orderBy("createdAt", "asc"));
    onSnapshot(q, (doc) => {
      setAllMessages(doc.docs.map((doc) => ({...doc.data(), id: doc.id})));
    
    });
  };
  getMessages();
}, []);


/* zorg ervoor dat er een conditie wordt gemaakt van welke user de userName is en welke de toUserName*/
const sendMessageToUser = async () => {
const messageCollection = collection(db, "friendships" , props.id, "messages")

if(newCurrentUser.displayName == props.userName) {
  await addDoc(messageCollection, {

    messageText: messageInput,
    messageName: newCurrentUser.displayName,
    messageUserId: newCurrentUser.uid,
    messagePhotoUrl: newCurrentUser.photoURL,
    messageToPhotoUrl: props.toUserPhotoUrl,
    messageToUserName: props.toUserName,
    messageToUserId: props.toUserId,
   
    createdAt: new Date()
    
    
    })
} else {

await addDoc(messageCollection, {

messageText: messageInput,
messageName: newCurrentUser.displayName,
messageUserId: newCurrentUser.uid,
messagePhotoUrl: newCurrentUser.photoURL,
messageToPhotoUrl: props.userPhotoUrl,
messageToUserName: props.userName,
messageToUserId: props.userId,

createdAt: new Date()


})
}
console.log('gelukt')
setMessageInput("")

}

  return (
    <Page name="Messages" className="scrollMessages">

<Navbar  title={newCurrentUser != null && newCurrentUser.displayName == props.toUserName ? props.userName : props.toUserName}>
              <NavRight>
                <Link back>Close</Link>
              </NavRight>
            </Navbar>
      <BlockTitle className="paddingTop">{newCurrentUser != null && newCurrentUser.displayName == props.toUserName ? props.userName : props.toUserName}</BlockTitle>


      <Messages  >


        
        <MessagesTitle>
          <b>Date of friendship: </b>{props.createdAt.toDate().toLocaleString()}
        </MessagesTitle>

     
        {allMessages.map((message) => { return(function()  {
                    try {
          if (newCurrentUser != null && newCurrentUser.uid == message.messageUserId) {
       return <Message key={message.id} header="myself" > 
       {message.messageText} 
      </Message>
          } else {
           return <div key={message.id}>
           <img className="userTextImage" src={message.messagePhotoUrl} />
           <Message  header={message.messageName} className="usermessagestext" type="received">
            {message.messageText} 
           </Message></div>; 
          }
        }
        catch{
          f7.dialog.alert('Login now!')
        }
        })()   

})}
          
           

      </Messages>

      <List noHairlinesMd  className="messageFixedPosition" >
 
   <ListInput    clearButton placeholder="Type Your Message " value={messageInput}  onChange={(event) => {setMessageInput(event.target.value)}}>
</ListInput>

<Button className="placeAddFriendCol"  fill onClick={() => sendMessageToUser()}  >Send Message </Button>
    </List>
 
    
    </Page>
      
    

   
  );
}

export default UserMessagesPage;
