import React from 'react';
import {
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  NavTitleLarge,
  Link,
  Fab,
  Icon,
  BlockTitle,

} from 'framework7-react';
import OnlineUserSlider from '../components/onlineUsersSlider';
import PostsCards from '../components/postCards';




    const HomePage = () => {
  

  return(
  <Page name="home">

    
    {/* Top Navbar */}
    <Navbar large sliding={false}>
      <NavLeft>
        <Link iconIos="f7:menu" iconAurora="f7:menu" iconMd="material:menu" panelOpen="left" />
      </NavLeft>
      <NavTitle sliding>BroMeUI</NavTitle>
    
      <NavTitleLarge>BroMeUI</NavTitleLarge>
    </Navbar>

    <BlockTitle>Online People</BlockTitle>

    <OnlineUserSlider/>

    <BlockTitle>Posts</BlockTitle>
 
     <PostsCards/>

     <Fab href='/post/' position="right-bottom" slot="fixed"  text="Create" color="#2f5a88" >
      <Icon ios="f7:plus" aurora="f7:plus" md="material:add"></Icon>
 
    </Fab>


  </Page>
)};


export default HomePage;