import React from 'react';
import { Page, Navbar, Searchbar , BlockTitle} from 'framework7-react';
import FriendList from '../components/friendList';


const FriendPage = () => {
 
  return (<Page name="friends">
  <Navbar title="Friends" />
    <Searchbar
        searchContainer=".search-list"
        searchIn=".item-title"
        type="text"
        placeholder="Search For Friend"
        clearButton></Searchbar>
          <BlockTitle>Your Friends</BlockTitle>

          
          <FriendList/>
 

 </Page>
 
  );
}

export default FriendPage;
