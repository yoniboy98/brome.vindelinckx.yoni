import React, {useState, useEffect, useContext, useRef} from 'react';
import {
  Swiper,
  SwiperSlide,
  Row,
  ListItem,
  Page,
  Navbar,
  NavRight,
  BlockTitle,
  List,
  AccordionContent,
  Block,
  Button,
  PhotoBrowser,
  Link,
  Col,
  Popup,
 
} from 'framework7-react';
import { initializeApp } from 'firebase/app';
import { getFirestore, onSnapshot,collection,query, where, doc, getDocs} from 'firebase/firestore';
import userProfile from './userProfile';
import { AuthContext} from '../context/Auth';


const firebaseConfig = {
apiKey: "AIzaSyAi2IBMGJ_En3_hIRha4wwAVkSu7nPVraY",
authDomain: "brome-5e406.firebaseapp.com",
databaseURL: "https://brome-5e406-default-rtdb.europe-west1.firebasedatabase.app",
projectId: "brome-5e406",
storageBucket: "brome-5e406.appspot.com",
messagingSenderId: "130755062271",
appId: "1:130755062271:web:afdd4c7264793564de5fca",
measurementId: "G-ZBXLWKV8L4"
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);




 
const OnlineUserSlider = () => {

const [onlineUsers, setOnlineUsers] = useState([])
const [popupOpened, setPopupOpened] = useState(false);

const [userId, setUserId]= useState()

const [userName, setUserName] = useState("");

const [userEmail, setUserEmail] = useState("");

const [userPhotoUrl, setUserPhotoUrl] = useState();

const [aboutOnId, setAboutOnId] = useState("");

const [phonenumberOnId, setPhonenumberOnId] = useState("");

const [workOnId, setWorkOnId] = useState("");

const [genderOnId, setGenderOnId] = useState("");

const [birthOnId, setBirthOnId] = useState("");

const [livingOnId, setLivingOnId] = useState("");

const [profilePictureOnId,   setProfilePictureOnId] = useState();

const [profilePictureOnIdTwo,   setProfilePictureOnIdTwo] = useState();




const [disable, setDisable] = useState(false);

const [disableDelete, setDisableDelete] = useState(false);


const {newCurrentUser} = useContext(AuthContext)



const [friendHandler, setFriendHandler] = useState(false)

const [ friendshipOnId, setFriendshipId] = useState()


const getUserDataOnClick = async (userId) => {


    onSnapshot(doc(db, "users", userId), (doc) => {
      
   setUserId(doc.data().userId)
   setUserName(doc.data().userName)
   setUserEmail(doc.data().userEmail)
   setUserPhotoUrl(doc.data().photoUrl)
   setAboutOnId(doc.data().userAbout)
   setPhonenumberOnId(doc.data().userPhoneNumber)
   setWorkOnId(doc.data().userWork)
   setGenderOnId(doc.data().userGender)
   setBirthOnId(doc.data().userBirth)
   setLivingOnId(doc.data().userLiving)
   setProfilePictureOnId(doc.data().profileImage)
   setProfilePictureOnIdTwo(doc.data().profileImageTwo)
    });

 

    try {
    if(userId == newCurrentUser.uid) {
        setDisable(true)
    } else {
      setDisable(false)
    }
  } catch {
    f7.dialog.alert('You need to Login First !');
    setDisable(true)
  }


  const q = query(collection(db, "friendships"));
  const querySnapshot = await getDocs(q);
  querySnapshot.forEach((doc) => {

try {

if(doc.data().userId == newCurrentUser.uid || doc.data().toUserId == newCurrentUser.uid) {
  setDisable(true)


}
 if(doc.data().isFriend == true) {
   setFriendHandler(doc.data().isFriend)
   setFriendshipId(doc.id)
   setDisableDelete(false)
}
} 

catch {

}



});
}


const handleFriendRequestButton = () => {

    if(friendHandler == true && newCurrentUser && newCurrentUser.uid != userId && friendshipOnId != null) {
   
      return  <Button className="placeAddFriendCol" disabled={disableDelete} color="red" fill onClick={() => DeleteFriendshipOnId(friendshipOnId)}>
        Delete As Friend</Button>
  

    } else {
      
      return <Button fill color="blue" disabled={disable} popupClose={true}  className="placeAddFriendCol" onClick={() => SendFriendshipRequestToUser(userId)}>
             Friend Request
            </Button>
    }
    
  }



const DeleteFriendshipOnId = async () => {

  await deleteDoc(doc(db, "friendships", friendshipOnId ));
  f7.dialog.alert('Friendship  removed!');
  setDisableDelete(true)
}



  const  SendFriendshipRequestToUser = async () => {
    const FriendCollection = collection(db, "friendships")
  

    await addDoc(FriendCollection, {
   userName: newCurrentUser.displayName,
   userId: newCurrentUser.uid,
   userPhotoUrl: newCurrentUser.photoURL,
   toUserId: userId,
   toUserName: userName,
   toUserPhotoUrl: userPhotoUrl,
   isFriend: false,
   createdAt: new Date()
   

    })
    console.log("gelukt")
  
  }




useEffect(() => {
 
  const getOnlineUsers = () => {
  const q = query(collection(db, "users"), where("isOnline", "==", true));
  onSnapshot(q, (doc) => {
 
    setOnlineUsers(doc.docs.map((doc) => ({...doc.data(), id: doc.id})));  
  });
}
getOnlineUsers();
  }, []);


  
  const standalone = useRef(null);

  const photos = [userPhotoUrl,profilePictureOnIdTwo ? profilePictureOnIdTwo  : 'https://placekitten.com/800/800', profilePictureOnId ? profilePictureOnId : 'https://placekitten.com/1024/1024' ];




   
    return(
<Swiper  speed={300} slidesPerView={5} >
 <Row id ="onlinePeopleListHome">
 {onlineUsers.map((online) => { return  <SwiperSlide key={online.id}>
   <Col>
   <ListItem link routeProps={online} popupOpen="#my-profilPopup" onClick={() => getUserDataOnClick(online.id)} >
     <img className="onlineImage"
         src={online.photoUrl}
         width="64"
         height="64"
   
       />
       <p className="onlineName">{online.userName}</p></ListItem> </Col>
   </SwiperSlide>

})}

   </Row>
   <Popup id="my-profilPopup"
  opened={popupOpened}
  onPopupClosed={() => setPopupOpened(false)}>
    
            <Navbar title="Profile">
              <NavRight>
                <Link popupClose>Close</Link>
              </NavRight>
            </Navbar>

            <Page>
<BlockTitle>Profile</BlockTitle>
            <div className="gridProfile">
            <img className="userProfileImage" src={userPhotoUrl == null ? "https://placekitten.com/800/800" : userPhotoUrl} onClick={() => standalone.current.open()} width="45%" height="30%"/>
      
            <BlockTitle>{userName == "" ? "Log in voor een eigen profiel" : userName}</BlockTitle>
            </div>
            <List >
              
            <ListItem accordionItem title="ABOUT ME" className="fontStyleUserProfile">
        <AccordionContent>
          <Block>
            <p>
            {aboutOnId == "" ? 'nothing to say about me :/' : aboutOnId}
            </p>
          </Block>
        </AccordionContent>
      </ListItem>
        
            <ListItem accordionItem title="CONTACTDATA" className="fontStyleUserProfile">
        <AccordionContent>
          <List>
          <ListItem title="Woonplaats">{livingOnId == "" ? 'On earth' : livingOnId}</ListItem>
          <ListItem title="email">{userEmail == "" ? 'email.email@login.be' : userEmail}</ListItem>
          <ListItem title="telefoonnummer">{phonenumberOnId == "" ? 'log in for your number so the girls can call you ;)' : phonenumberOnId}</ListItem>
          </List>
        </AccordionContent>
      </ListItem>

      <ListItem accordionItem title="ALGEMENE GEGEVENS" className="fontStyleUserProfile">
        <AccordionContent>
          <List>
          <ListItem title="geslacht">{genderOnId == "" ? '1 of the ...' : genderOnId}</ListItem>
          <ListItem title="studie/werk">{workOnId == "" ? 'I dont now what work is' : workOnId}</ListItem>
          <ListItem title="geboortedatum">{birthOnId == "" ? 'God made me' : birthOnId}</ListItem>
          </List>
        </AccordionContent>
      </ListItem>



      <ListItem accordionItem title="RELATIE" className="fontStyleUserProfile">
        <AccordionContent>
          <List>
            <ListItem title="In relatie">ja</ListItem>
            <ListItem title="naam persoon">een hond</ListItem>
          </List>
        </AccordionContent>
      </ListItem>
       

      <ListItem accordionItem title="FAVORITE GAMES/APPS" className="fontStyleUserProfile">
        <AccordionContent>
          <List>
            <ListItem title="games">cod</ListItem>
            <ListItem title="apps">BroMe</ListItem>
            <ListItem title="site">www.myhostingname.be</ListItem>
            <ListItem title="bordspel">schaken</ListItem>
          </List>
        </AccordionContent>
      </ListItem>

      <ListItem accordionItem title="SPORT AND HOBBY'S" className="fontStyleUserProfile">
        <AccordionContent>
          <List>
          <ListItem title="sport">voetbal</ListItem>
          <ListItem title="hobby's">mensen in elkaar slaan</ListItem>
          </List>
        </AccordionContent>
      </ListItem>


      <Row>
        <Col> 

        {handleFriendRequestButton()}
         
            </Col>
       
      </Row>

      <PhotoBrowser photos={photos} ref={standalone} />
         
    </List>
    </Page>

            



     </Popup>
 </Swiper>


)};

export default OnlineUserSlider;