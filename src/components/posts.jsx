import React, { useState, useContext} from 'react';
import {
    Page,
    Navbar,
    NavRight,
    BlockTitle,
    List,
    ListInput,
    Block,
    Progressbar,
    Button,
    f7,
    Input,
    Link

} from 'framework7-react';
 import { initializeApp } from 'firebase/app';
import { getFirestore,addDoc, collection} from 'firebase/firestore';
import { getStorage, ref ,uploadBytesResumable, getDownloadURL, uploadString, uploadBytes} from "firebase/storage";
import { AuthContext } from '../context/Auth';


const firebaseConfig = {
    apiKey: "AIzaSyAi2IBMGJ_En3_hIRha4wwAVkSu7nPVraY",
  authDomain: "brome-5e406.firebaseapp.com",
  databaseURL: "https://brome-5e406-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "brome-5e406",
  storageBucket: "brome-5e406.appspot.com",
  messagingSenderId: "130755062271",
  appId: "1:130755062271:web:afdd4c7264793564de5fca",
  measurementId: "G-ZBXLWKV8L4"
  };
  
  
  const db = getFirestore(app);
  const app = initializeApp(firebaseConfig);
  const storage = getStorage(app);


  const posts = () => {



  const [file, setFile] = useState("");

  const [url, setUrl] = useState("");

  const [isProgress, setProgress] = useState(0);

  const [disable, setDisable] = useState(false);

  const [newTitle, setTitle ] = useState("");

  const [newText, setText ] = useState("");

  const {newCurrentUser} = useContext(AuthContext)



  /*voeg een post toe aan de muur*/ 
  const sendPost =  async () => {
  
    const postCollection = collection(db, "Posts")
    await addDoc(postCollection, {
      Title: newTitle,
      Text: newText,
      userEmail: newCurrentUser.email,
      userName: newCurrentUser.displayName,
      userId: newCurrentUser.uid,
      photoUrlUser: newCurrentUser.photoURL,
      likes: 0,
      comments: 0,
      postImage: url,
      createdAt: firebase.firestore.Timestamp.fromDate(new Date()).toDate()
   

    })
    .then(() => {
      f7.dialog.alert('de post, is toegevoegd!');
      setText("")
      setTitle("")
  
      //alle inputfields leegmaken
      //met clearButton dat bij elke ListInput staat
    })
    .catch(() => {
      f7.dialog.alert('de post verzenden is niet gelukt, probeer opnieuw');
      return;
    });
  };






  
  function handleChange(e) {
    setFile(e.target.files[0]);
  
  }



  const uploadImage = () => {

  
const storageRef = ref(storage, '/postImages/' + file.name);
 
const metadata = {
  contentType: 'image/jpeg',
};

// Upload the file and metadata
const uploadTask = uploadBytesResumable(storageRef, file, metadata);
      
uploadTask.on('state_changed', 
(snapshot) => {
  // Observe state change events such as progress, pause, and resume
  // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
  const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
  setProgress(progress)
  switch (snapshot.state) {
    case 'paused':
      console.log('Upload is paused');
      break;
    case 'running':
      console.log('Upload is running');
      break;
  }
}, 
(error) => {
  // Handle unsuccessful uploads
}, 
() => {
  // Handle successful uploads on complete
  // For instance, get the download URL: https://firebasestorage.googleapis.com/...
  getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
    setUrl(downloadURL)
     setDisable(true)

  });
}
);

  }




    return( <Page>
        <Navbar title="Create Posts">
          <NavRight>
          <Link back>Close</Link>
          </NavRight>
        </Navbar>
        <BlockTitle>Say something ;)</BlockTitle>

        <List noHairlinesMd>
              
              <ListInput
              outline
      label="Hey !"
      floatingLabel
      type="textarea"
      resizable
      value={newTitle}
      placeholder="Enter your subject here."
      onChange={(event) => {setTitle(event.target.value)}}>
    </ListInput>
    
   </List>
   <List noHairlinesMd>
   <ListInput
              outline
      label="Tell something funny :D !"
      floatingLabel
      type="textarea"
      resizable
      value={newText}
      placeholder="Enter your subject here."
      onChange={(event) => {setText(event.target.value)}}>
  
    
    </ListInput>
    </List>
    <Block>
        <Input type="file" accept="image/*" capture="camera" onChange={handleChange} className="inputtypeImage"></Input>
        <Button onClick={uploadImage } disabled={disable}>Upload image</Button>
        <p>{isProgress} % </p>
            <Progressbar color="blue" progress={isProgress} />
  
    
            <div >
              <img src={url} className="imgPost"></img>
            </div>
      
              <Button onClick={sendPost}  className="placePostCol" raised fill>Place Post</Button>
</Block>
        </Page>


    )}


  export default posts
  