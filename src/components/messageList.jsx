import React, { useState, useEffect , useContext} from 'react';
import {
  List,
  ListItem,
  SwipeoutActions,
  SwipeoutButton,
} from 'framework7-react';
import { initializeApp } from 'firebase/app';
import { getFirestore,query,where, collection, onSnapshot} from 'firebase/firestore';
import { AuthContext } from '../context/Auth';


const firebaseConfig = {
    apiKey: "AIzaSyAi2IBMGJ_En3_hIRha4wwAVkSu7nPVraY",
  authDomain: "brome-5e406.firebaseapp.com",
  databaseURL: "https://brome-5e406-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "brome-5e406",
  storageBucket: "brome-5e406.appspot.com",
  messagingSenderId: "130755062271",
  appId: "1:130755062271:web:afdd4c7264793564de5fca",
  measurementId: "G-ZBXLWKV8L4"
  };
  
  
  const db = getFirestore(app);
  const app = initializeApp(firebaseConfig);




const MessageList = () => {

    const {newCurrentUser} = useContext(AuthContext)



  const [friends, setFriends] = useState([]);


  //friends == true ophalen

  useEffect(() => {
    const getFriends = async () => {

    const q = query(collection(db, "friendships") ,where("isFriend", "==", true));
      onSnapshot(q, (doc) => {
        setFriends(doc.docs.map((doc) => ({...doc.data(), id: doc.id})));

      });
  
    
    };


    getFriends();
  }, []);








  return(
 
    <List className="search-list2 searchbar-found">
   
        
    {friends.map((friend) => { return <ListItem swipeout link="/userMessages/:id/" key={friend.id} routeProps={friend} badge="Friend" badgeColor="yellow"
      title={newCurrentUser != null && newCurrentUser.displayName == friend.toUserName ? friend.userName : friend.toUserName} >
        <SwipeoutActions right>
 
  
            <SwipeoutButton delete confirmText="Are you sure you want to delete this message?">
              Delete
            </SwipeoutButton>
            
          </SwipeoutActions>
 
          </ListItem>
 })}
        
 
      
 
 
 
      
    </List>



  )}






export default MessageList;
