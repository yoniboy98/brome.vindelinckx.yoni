import React, {useState, useEffect, useContext} from 'react';
import {
    Page,
    Navbar,
    NavRight,
    Link,
    CardHeader,
    CardContent,
    CardFooter,
    f7,
    Swiper,
    SwiperSlide,
    BlockTitle,
    Popup,
    List,
    ListItem,
    View,
    ListInput,
    Messages,
    MessagesTitle,
    Message,
    Card,
    Button
 
} from 'framework7-react';
 {/* Page content */}

 import { initializeApp } from 'firebase/app';
import { getFirestore, onSnapshot,doc, deleteDoc,query, collection, addDoc,updateDoc,orderBy,increment, getDocs,setDoc, serverTimestamp } from 'firebase/firestore';
import { AuthContext} from '../context/Auth';




const firebaseConfig = {
  apiKey: "AIzaSyAi2IBMGJ_En3_hIRha4wwAVkSu7nPVraY",
authDomain: "brome-5e406.firebaseapp.com",
databaseURL: "https://brome-5e406-default-rtdb.europe-west1.firebasedatabase.app",
projectId: "brome-5e406",
storageBucket: "brome-5e406.appspot.com",
messagingSenderId: "130755062271",
appId: "1:130755062271:web:afdd4c7264793564de5fca",
measurementId: "G-ZBXLWKV8L4"
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);





const postsCards = () => {


    const [posts, setPosts] = useState([]);
    

    const [comment, setComment] = useState("")

    const [postId , setPostid] = useState()

   
    const [allComments, setAllComments] = useState([])

    const [userLikeId, setUserLikeId] = useState()


  const {newCurrentUser} = useContext(AuthContext)

  const [disabled, setDisabled] = useState(false)
    


   const getPostId = (id) => {

     setPostid(id)
 
     const q = query(collection(db, "Posts", id, "comments"),orderBy("createdAt", "asc"));
      onSnapshot(q,(doc) => {
        setAllComments(doc.docs.map((doc) => ({...doc.data(), id: doc.id})));
 

      });
    
   }




   const sendCommentsOnPostId = (id) => {

  
   const commentCollection = collection(db, "Posts", id , "comments")
 
    addDoc(commentCollection, {

  commentText: comment,
  commentName: newCurrentUser.displayName,
  commentUserId: newCurrentUser.uid,
  commentUserPhotoUrl: newCurrentUser.photoURL,
  postId: postId,
  createdAt: new Date()


   }).then(() => {

    setComment("")
    const commentCountCollection = doc(db, "Posts", id)
  
    updateDoc(commentCountCollection, {
 
     comments: increment(1)
   
    
      })
   console.log('gelukt');

   }).catch(() => {
     console.log('probleme')
   })
  
  }

  
  
const [likeUpdated, setLikeUpdated] = useState(false)




  /*decrement likes op id*/ 
const decrementLikes = (id) => {

let counter = likeSize

   deleteDoc(doc(db, "Posts", id , "likes" , newCurrentUser.uid));

  const likeCountCollection = doc(db, "Posts", id)
 
  updateDoc(likeCountCollection, {

   likes: counter

    }).then(()=> {
      setLikeUpdated(false)

    })
 
}

 


  /*increment likes op id*/ 
  const incrementLikes = async (id) =>  {

 
    setDoc(doc(db, "Posts", id , "likes" , newCurrentUser.uid), {
      likeId: newCurrentUser.uid,
      likeName: newCurrentUser.displayName,
      createdAt: new Date()
  
   
    }).then(() => {

    
      const likeCountCollection = doc(db, "Posts", id)
 
      updateDoc(likeCountCollection, {
    
       likes: increment(1)
     
        }).then(() => {
        setLikeUpdated(true)
     
      })
  

    })
 }




 const [newDocId, setNewDocId] = useState()
 const [likeSize, setLikeSize] = useState()
 


     //posts ophalen
     useEffect(() => {
      const getPosts = async () => {
  
       const q = query(collection(db, "Posts"),orderBy("createdAt", "desc"));
        onSnapshot(q, (doc) => {
          setPosts(doc.docs.map((doc) => ({...doc.data(), id: doc.id})));
 
         /* doc.forEach((doc) => {
    
         setNewDocId(doc.id)


         const q = collection(db, "Posts", doc.id , "likes");
           onSnapshot(q, (doc) => {
            setLikeSize(doc.size)
    
             doc.forEach((doc) => {

              
              if(newCurrentUser && newCurrentUser.uid == doc.id) {
                setLikeUpdated(true)
    
 
                      }else {
                        setLikeUpdated(false)
         
                      }
           
             })
           });
         
          })*/
      
        });
 
  
       
      };
      getPosts();
    }, []);
    


 
   
     //post verwijderen
  const deletePost = async (id) => {
    await deleteDoc(doc(db, "Posts", id));
      f7.dialog.alert('Thanks, post removed!');
  };





  

    return (<List> 
        {posts.map((post) => { return <Card className="demo-facebook-card" key={post.id}>
                <CardHeader className="no-border">
                  <div className="demo-facebook-avatar">
                    <img 
                      src={post.photoUrlUser}
                      width="60"
                      height="70"
                    />
                  </div>
                  <div className="demo-facebook-name">{post.userName}</div>
                  <div className="demo-facebook-date">{post.Title}</div>
                </CardHeader>
                <CardContent>
                  <p>{post.Text}</p>
                  <img src={post.postImage} width="100%" />
                  <p className="likes">Likes: {post.likes} Comments: {post.comments}</p>
                </CardContent>
                <CardFooter className="no-border">


                {likeUpdated == false ? <Button onClick={() => incrementLikes(post.id)} > Like</Button> : <Button onClick={() => decrementLikes(post.id)} >Dislike</Button>  }
                  




                  
                  <Link popupOpen="#CommentPopup"   onClick={ ()=> getPostId(post.id)}>Comments</Link>


                  {(function() {
                    try {
          if (newCurrentUser != null && newCurrentUser.uid == post.userId) {
       return <Link onClick={() => deletePost(post.id)}>Verwijderen</Link>;
          } else {
            return <p className="fontSizePostsDate">Date: {post.createdAt.toDate().toLocaleString()}</p>;  
          }
        }
        catch{
          f7.dialog.alert('Login now!')
        }
        })()}


            
                </CardFooter>








                <Popup id="CommentPopup">
        <View>
          <Page className="scrollMessages">
            <Navbar title="Comments">
              <NavRight>
                <Link popupClose>Close</Link>
              </NavRight>
            </Navbar>
        
            <List mediaList slot="fixed" >
       
            <ListInput className="commentFixedPosition"
    label="What's your opinion?"
    type="textarea"
    resizable
    value={comment}
    placeholder="Send Comment"
    onChange={(event) => {setComment(event.target.value)}}
  >
 
  </ListInput>
  </List>
  <Button  fill color="green" slot="fixed" className="placePostCol" onClick={()=> sendCommentsOnPostId(postId)}>
            Send Comment
            </Button>

       
            <BlockTitle className="paddingTop" >All Comments</BlockTitle>
          
            <Messages >
        <MessagesTitle>
          <b>Sunday, Feb 9,</b> 12:58
        </MessagesTitle>



        {allComments.map((all) => { return(function() {
                    try {
          if (newCurrentUser != null && newCurrentUser.uid == all.commentUserId) {
       return <Message key={all.id} header={all.commentName}>
       {all.commentText} 
      </Message>
          } else {
            
           return <div key={all.id}>
           <img className="userTextImage" src={all.commentUserPhotoUrl} />
            <Message className="usermessagestext" header={all.commentName} type="received">
            {all.commentText} 
           </Message></div>; 
          }
        }
        catch{
          f7.dialog.alert('Login now!')
        }
        })()   

})}


      </Messages>
     
    

          </Page>
        </View>
      </Popup>




              </Card>
              
              })}



          
   

              </List>
        

    )};

export default postsCards;