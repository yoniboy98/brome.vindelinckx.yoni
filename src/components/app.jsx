import React, { useState, useEffect , useRef, useContext} from 'react';
import { getDevice }  from 'framework7/lite-bundle';
import {
  f7,
  f7ready,
  App,
  Panel,
  Views,
  View,
  PhotoBrowser,
  Popup,
  Button,
  Page,
  Badge,
  Navbar,
  Toolbar,
  Row,
  Col,
  Gauge,
  Progressbar,
  Icon,
  NavRight,
  Link,
  Block,
  BlockHeader,
  AccordionContent,
  TextEditor,
  BlockTitle,
  List,
  ListItem,
  Tabs,
  Toggle,
  Tab,
  ListInput,
  ListButton,
  BlockFooter,
  Input,
  NavTitle
} from 'framework7-react';
import cordovaApp from '../js/cordova-app';


import routes from '../js/routes';
import store from '../js/store';
import { on } from 'dom7';
import { AuthProvider } from '../context/Auth';
import { initializeApp } from 'firebase/app';
import { getFirestore,addDoc, collection, updateDoc,deleteDoc,onSnapshot, doc, getDocs, query, where} from 'firebase/firestore';
import { getAuth, signInWithPopup, GoogleAuthProvider, signOut,setPersistence,browserLocalPersistence, inMemoryPersistence, browserSessionPersistence,signInWithRedirect } from "firebase/auth";
import { getStorage, ref ,uploadBytesResumable, getDownloadURL, uploadString, uploadBytes} from "firebase/storage";



const firebaseConfig = {
  apiKey: "AIzaSyAi2IBMGJ_En3_hIRha4wwAVkSu7nPVraY",
authDomain: "brome-5e406.firebaseapp.com",
databaseURL: "https://brome-5e406-default-rtdb.europe-west1.firebasedatabase.app",
projectId: "brome-5e406",
storageBucket: "brome-5e406.appspot.com",
messagingSenderId: "130755062271",
appId: "1:130755062271:web:afdd4c7264793564de5fca",
measurementId: "G-ZBXLWKV8L4"
};


const db = getFirestore(app);
const app = initializeApp(firebaseConfig);
const storage = getStorage(app);


const MyApp = () => {
 

  const device = getDevice();
  // Framework7 Parameters
  const f7params = {
    name: 'BroMeUI', // App name
      theme: 'auto', // Automatic theme detection


      id: 'io.framework7.BroMeUI', // App bundle ID
      // App store
      store: store,
      // App routes
      routes: routes,
      


      // Input settings
      input: {
        scrollIntoViewOnFocus: device.cordova && !device.electron,
        scrollIntoViewCentered: device.cordova && !device.electron,
      },
      // Cordova Statusbar settings
      statusbar: {
        iosOverlaysWebView: true,
        androidOverlaysWebView: false,
      },
  };

  f7ready(() => {
    // Init cordova APIs (see cordova-app.js)
    if (f7.device.cordova) {
      cordovaApp.init(f7);
     
    }
  });





  const [friendshipRequests, setFriendshipRequest] = useState([])

  const [sizeFriendships, setSizeFriendships] = useState()

  const [newUser, setUser] = useState();

const [disable, setDisable] = useState(false)
 

  /* user data */

  const [userId, setUserId]= useState()

  const [userName, setUserName] = useState("");

  const [userEmail, setUserEmail] = useState("");

  const [userPhotoUrl, setUserPhotoUrl] = useState('https://placekitten.com/1024/1024');

  

  /*raport user*/ 
  

  const [nameOfUserGetsRaported, setNameOfUserGetsRaported] = useState("");
  const [reasonWhy, setReasonWhy] = useState("");
  const [textOfUserRaporter, setTextOfUserRaporter] = useState("");


    
  /* data of user on ID*/
  const [aboutOnId, setAboutOnId] = useState("");

  const [phonenumberOnId, setPhonenumberOnId] = useState("");

  const [workOnId, setWorkOnId] = useState("");

  const [genderOnId, setGenderOnId] = useState("");

  const [birthOnId, setBirthOnId] = useState("");

  const [livingOnId, setLivingOnId] = useState("");




  // profile image
  const [file, setFile] = useState("");

  const [file2, setFile2 ] = useState("")

  const [url, setUrl] = useState("");
  
  const [newUrl, setNewUrl] = useState("");

  const [url2, setUrl2] = useState("");

  const [newUrl2, setNewUrl2] = useState("")

  const [isProgress, setProgress] = useState(0);

  const [isProgress2, setProgress2] = useState(0);


  const [profilePictureOnId,   setProfilePictureOnId] = useState();

  const [profilePictureOnIdTwo,   setProfilePictureOnIdTwo] = useState();

  const authObject =  () => {
   
    const auth = getAuth()

    setPersistence(auth, browserLocalPersistence)
    .then( async () => {
      const provider = new GoogleAuthProvider();

      
      return signInWithPopup(auth, provider).then((result) => {
        // This gives you a Google Access Token. You can use it to access the Google API.
        const credential = GoogleAuthProvider.credentialFromResult(result);
        const token = credential.accessToken;
        // The signed-in user info.
        const user = result.user;
       
        
        if (user) {
      
          setUser(user)
          setUserName(user.displayName)
          setUserEmail(user.email)
          setUserPhotoUrl(user.photoURL)
          setUserId(user.uid)
          setTapDisabled(false)
   
      

          // vriendschap verzoeken ophalen
          const q = query(collection(db, "friendships"),where("toUserId", "==", user.uid), where("isFriend", "==", false));
          onSnapshot(q, (doc) => {
            setFriendshipRequest(doc.docs.map((doc) => ({...doc.data(), id: doc.id})));
            setSizeFriendships(doc.size)
           
            
         
          });

  } else {
 
  }

    })
  
 }).catch((error) => {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;
        // The email of the user's account used.
        const email = error.email;
        // The AuthCredential type that was used.
        const credential = GoogleAuthProvider.credentialFromError(error);
        // ... 
      })
    }


    const handleAuthButton = () => {

      if(userId != null) {
     
        return  <Button className="placeAddFriendCol" onClick={logout} fill >Logout</Button>
    
  
      } else {
        
        return  <Button  className="placeAddFriendCol" fill onClick={authObject}>Login With Google</Button>
      }
      
    }

  
    useEffect( () => { 
       if(userId == null){
        setDisable(true)
        setAboutOnId("")
        setPhonenumberOnId("")
        setWorkOnId("")
        setGenderOnId("")
        setBirthOnId("")
        setLivingOnId("")
        setProfilePictureOnId("https://placekitten.com/1024/1024")
        setProfilePictureOnIdTwo("https://placekitten.com/800/800")
      

       } else {
        setDisable(false)
       }

  
      
    });

    const logout = () => {

      const auth = getAuth()

   
      auth.signOut().then(function() {

           setUser("")
          setUserName("")
          setUserEmail("")
          setUserPhotoUrl('https://placekitten.com/1024/1024')
          setUserId()
          setFriendshipRequest([]);
          setSizeFriendships(0)
          setTapDisabled(true)
          setToggleOnlineChecked(false)
       
          const setUserOfflineDoc = doc(db, "users", userId )
  
          updateDoc(setUserOfflineDoc, {
          isOnline: false,
         
     
         })
         
  
      }).catch(function(error) {
        // An error happened.
      
      });
    
    }

  
  
    
    useEffect( async () => {
    
      if(userId != null) {

        const q = query(collection(db, "users"), where("userId", "==", userId));
         onSnapshot(q, (querySnapshot) => {
          querySnapshot.forEach((doc) => {
            
            setAboutOnId(doc.data().userAbout)
            setPhonenumberOnId(doc.data().userPhoneNumber)
            setWorkOnId(doc.data().userWork)
            setGenderOnId(doc.data().userGender)
            setBirthOnId(doc.data().userBirth)
            setLivingOnId(doc.data().userLiving)
            setProfilePictureOnId(doc.data().profileImage)
            setNewUrl(doc.data().profileImage)
            setProfilePictureOnIdTwo(doc.data().profileImageTwo)
            setNewUrl2(doc.data().profileImageTwo)
            if(doc.data().isOnline == true) {
              setToggleOnlineChecked(true)
            }



          });
         
        });


    }
  
    });




    function  handleChange2(e) {
      setFile2(e.target.files[0]);
    
    }
    function handleChange(e) {
      setFile(e.target.files[0]);
    
    }

    const uploadImage = () => {

  
      const storageRef = ref(storage, '/profilePicture/' + file.name);
       
      const metadata = {
        contentType: 'image/jpeg',
      };
      
      // Upload the file and metadata
      const uploadTask = uploadBytesResumable(storageRef, file, metadata);
            
      uploadTask.on('state_changed', 
      (snapshot) => {
        // Observe state change events such as progress, pause, and resume
        // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
        const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        setProgress(progress)
        switch (snapshot.state) {
          case 'paused':
            console.log('Upload is paused');
            break;
          case 'running':
            console.log('Upload is running');
            break;
        }
      }, 
      (error) => {
        // Handle unsuccessful uploads
      }, 
      () => {
        // Handle successful uploads on complete
        // For instance, get the download URL: https://firebasestorage.googleapis.com/...
        getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
          setUrl(downloadURL)
         
      
        });
      }
      );
      
        }

        const uploadImage2 = () => {

  
          const storageRef = ref(storage, '/profilePicture/' + file2.name);
           
          const metadata = {
            contentType: 'image/jpeg',
          };
          
          // Upload the file and metadata
          const uploadTask = uploadBytesResumable(storageRef, file2, metadata);
                
          uploadTask.on('state_changed', 
          (snapshot) => {
            // Observe state change events such as progress, pause, and resume
            // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
            const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            setProgress2(progress)
            switch (snapshot.state) {
              case 'paused':
                console.log('Upload is paused');
                break;
              case 'running':
                console.log('Upload is running');
                break;
            }
          }, 
          (error) => {
            // Handle unsuccessful uploads
          }, 
          () => {
            // Handle successful uploads on complete
            // For instance, get the download URL: https://firebasestorage.googleapis.com/...
            getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
              setUrl2(downloadURL)
             
          
            });
          }
          );
          
            }

        const addProfilePicture = () => {
        
          const userDocRef = doc(db, "users", userId )

          updateDoc(userDocRef, {
            profileImage: url,
        
       
           })
          }

           const addSecondProfilePicture = () => {
            
          const userDocRef = doc(db, "users", userId )

          updateDoc(userDocRef, {
            profileImageTwo: url2,
        
       
           })
           }



       
   

  const PermitFriendshipRequest = async (id) => {
    const userFriendShipsRequestDoc = doc(db, "friendships", id )
  
     updateDoc(userFriendShipsRequestDoc, {
     isFriend: true,
     createdAt: new Date()

    })
    .then(() => {
      f7.dialog.alert('Friendship request accepted');
  
   
    })
    .catch(() => {
      f7.dialog.alert('It was not possible to permit the friendshiprequest');
      return;
    });



  }

  const DenyFriendshipRequest = async (id) => {
    await deleteDoc(doc(db, "friendships", id ));
    f7.dialog.alert('Thanks, friendship request removed!');
};

  
  

  const openDialog = () => {
    f7.dialog.preloader("Loading...", "multi");
    setTimeout(() => {
      f7.dialog.close();
    }, 300);
  };




  /*raport user*/
  
  const raportUser =  async () => {
  
    const raportCollection = collection(db, "Raports")
    await addDoc(raportCollection, {
      nameOfRaporter: userName,
      nameOfUserGetsRaported:nameOfUserGetsRaported ,
      reasonWhy: reasonWhy,
      TextOfUserRaporter: textOfUserRaporter,
      createdAt: new Date()
  
    })
    .then(() => {
      f7.dialog.alert('de post, is toegevoegd!');
      setNameOfUserGetsRaported("")
      setTextOfUserRaporter("")
 
      //alle inputfields leegmaken
      //met clearButton dat bij elke ListInput staat
    })
    .catch(() => {
      f7.dialog.alert('de post verzenden is niet gelukt, probeer opnieuw');
      return;
    });
  };

  

  const updateOnlineStateOfUser = () => {

setToggleOnlineChecked(true)

if(toggleOnlineChecked == true) {
  
  const setUserOfflineDoc = doc(db, "users", userId )
  
  updateDoc(setUserOfflineDoc, {
  isOnline: false,
 

 })
  setToggleOnlineChecked(false)
}
if(toggleOnlineChecked == false) {
   
  const setUserOfflineDoc = doc(db, "users", userId )
  
  updateDoc(setUserOfflineDoc, {
  isOnline: true,
 

 })
}
  };





const [tapDisabled, setTapDisabled] = useState(true)
const [toggleOnlineChecked, setToggleOnlineChecked] = useState(false)

  const standalone = useRef(null);

  const photos = [profilePictureOnIdTwo ? profilePictureOnIdTwo : "https://placekitten.com/800/800", profilePictureOnId ? profilePictureOnId : "https://placekitten.com/1024/1024", userPhotoUrl];

  return (   <AuthProvider value={newUser}> 
    <App { ...f7params } >

        {/* Left panel with cover effect*/}
        <Panel left reveal themeDark>
          <View>
            <Page>
              <Navbar title="Menu"/>
            
              <List menuList mediaList className="menuMargin">


        <ListItem
          link
          title="Profile"
          popupOpen="#my-profilePopup"
          onClick={openDialog} >
          <Icon md="material:person" aurora="f7:person_fill" ios="f7:person_fill" slot="media" />
        </ListItem>

        <ListItem
          link
          title="Friendship Requests"
          popupOpen="#my-FriendshipRequestPopup"
          onClick={openDialog} >
        <Badge color="blue">{sizeFriendships == null ? 0 : sizeFriendships} requests </Badge>
          <Icon
            md="material:people"
            aurora="f7:person_2_fill"
            ios="f7:person_2_fill"
            slot="media"
            
          />
         
        </ListItem>

        <ListItem
          link
          title="Events"
          popupOpen="#my-EventsPopup"
          onClick={openDialog}  >
          <Icon
            md="material:star"
            aurora="f7:star_fill"
            ios="f7:star_fill"
            slot="media"
          />
        </ListItem>

        <ListItem
          link
          title="Raport User"
          popupOpen="#my-RaportPopup"
          onClick={openDialog} >
          <Icon
            md="material:block"
            aurora="f7:exclamationmark_square_fill"
            ios="f7:exclamationmark_square_fill"
            slot="media"
          />
        </ListItem>

     
       
 
    
      </List>
  
  
    
            </Page>
          </View>
        </Panel>


    

      
        {/* Views/Tabs container */}
        <Views tabs className="safe-areas">
          {/* Tabbar for switching views-tabs */}
          <Toolbar tabbar labels bottom>
            <Link tabLink="#view-home" tabLinkActive iconIos="f7:house_fill" iconAurora="f7:house_fill" iconMd="material:home" text="Posts" />
            <Link tabLink="#view-friends" iconIos="f7:person_3_fill" iconAurora="f7:person_3_fill" iconMd="f7:person_3_fill" text="Friends" />
            <Link tabLink="#view-messages"  iconIos="f7:chat_bubble_2_fill" iconAurora="f7:chat_bubble_2_fill" iconMd="f7:chat_bubble_2_fill" text="Messages" />
            
          </Toolbar>

          {/* Your main view/tab, should have "view-main" class. It also has "tabActive" prop */}
          <View id="view-home" main tab tabActive url="/" />

          {/* Catalog View */}
          <View id="view-friends" name="friends" tab url="/friends/" />

          {/* Settings View */}
          <View id="view-messages" name="messages" tab url="/settings/" />

        </Views>
     

       

  {/* Popup 
  show profile

  */}
  <Popup id="my-profilePopup">
        <View>
          <Page>
            <Navbar>
              <NavRight>
                <Link popupClose>Close</Link>
              </NavRight>
              <NavTitle> Profile </NavTitle>
            </Navbar>



            <Toolbar tabbar bottom>
  <Button tabLink="#profile" tabLinkActive>
  Profile
  </Button>
  <Button tabLink="#profileSettings" disabled={tapDisabled}>
  Settings
  </Button>

    </Toolbar>
    <Tabs>
    <Tab id="profile"  className="page-content" tabActive>
      <Block>
            <BlockTitle>Profile</BlockTitle>



            <div className="gridProfile">
            <img className="userProfileImage" src={userPhotoUrl == null ? "http://placekitten.com/g/200/300" : userPhotoUrl} onClick={() => standalone.current.open()} width="45%" height="30%"/>
      
            <BlockTitle>{userName == "" ? "Log in voor een eigen profiel" : userName}</BlockTitle>
            </div>


            <List >
            <ListItem accordionItem title="ABOUT ME" className="fontStyleUserProfile">
        <AccordionContent>
          <Block>
            <p>
            {aboutOnId == "" ? 'nothing to say about me :/' : aboutOnId}
            </p>
          </Block>
        </AccordionContent>
      </ListItem>
        
            <ListItem accordionItem title="CONTACTDATA" className="fontStyleUserProfile">
        <AccordionContent>
          <List>
          <ListItem title="Woonplaats">{livingOnId == "" ? 'On earth' : livingOnId}</ListItem>
          <ListItem title="email">{userEmail == "" ? 'email.email@login.be' : userEmail}</ListItem>
          <ListItem title="phone">{phonenumberOnId == "" ? '0123' : phonenumberOnId}</ListItem>
          </List>
        </AccordionContent>
      </ListItem>

      <ListItem accordionItem title="ALGEMENE GEGEVENS" className="fontStyleUserProfile">
        <AccordionContent>
          <List>
          <ListItem title="geslacht">{genderOnId == "" ? '1 of the ...' : genderOnId}</ListItem>
          <ListItem title="studie/werk">{workOnId == "" ? 'I dont now what work is' : workOnId}</ListItem>
          <ListItem title="geboortedatum">{birthOnId == "" ? 'God made me' : birthOnId}</ListItem>
          </List>
        </AccordionContent>
      </ListItem>



      <ListItem accordionItem title="RELATIE" className="fontStyleUserProfile">
        <AccordionContent>
          <List>
            <ListItem title="In relatie">ja</ListItem>
            <ListItem title="naam persoon">een hond</ListItem>
          </List>
        </AccordionContent>
      </ListItem>
       

      <ListItem accordionItem title="FAVORITE GAMES/APPS" className="fontStyleUserProfile">
        <AccordionContent>
          <List>
            <ListItem title="games">cod</ListItem>
            <ListItem title="apps">BroMe</ListItem>
            <ListItem title="site">www.myhostingname.be</ListItem>
            <ListItem title="bordspel">schaken</ListItem>
          </List>
        </AccordionContent>
      </ListItem>

      <ListItem accordionItem title="SPORT AND HOBBY'S" className="fontStyleUserProfile">
        <AccordionContent>
          <List>
          <ListItem title="sport">voetbal</ListItem>
          <ListItem title="hobby's">mensen in elkaar slaan</ListItem>
          </List>
        </AccordionContent>
      </ListItem>


      <Row> <Col>
      {handleAuthButton()}
      </Col>
    
      </Row>

  
      <PhotoBrowser theme="dark" photos={photos} ref={standalone} />
         


    </List>
    </Block>
    </Tab>
    <Tab id="profileSettings" className="page-content">


    <BlockTitle strong>Change status </BlockTitle>
    <Block>
<Row className="onlineToggleGrid">

        <BlockTitle>Offline </BlockTitle>
        <Toggle  checked={toggleOnlineChecked} disabled={disable} onToggleChange={() => updateOnlineStateOfUser()} color="orange"></Toggle>
        <BlockTitle>Online </BlockTitle>
    
</Row>
</Block>


<BlockTitle strong>Add new profile picture </BlockTitle>
<Row>
  <Col width="100" medium="50">
<Block>
 
<Input type="file" accept="image/*" capture="camera" onChange={handleChange} className="inputtypeImage"></Input>
<Button onClick={uploadImage } color="grey" >Upload Image</Button>

 
<p>{isProgress} % </p>
            <Progressbar color="blue" progress={isProgress} />
  
            <div>
              <img src={url ? url : newUrl} className="imgPost"></img>
            </div>

<Button onClick={addProfilePicture} fill color="green" className="placeAddFriendCol">Add To Profile </Button>

</Block>
</Col>
</Row>




 <Row>
 <Col width="100" medium="50">
   <Block>
<Input type="file" accept="image/*" capture="camera" onChange={handleChange2} className="inputtypeImage"></Input>
<Button onClick={uploadImage2 } color="grey" >Upload Image</Button>


<p>{isProgress2} % </p>
            <Progressbar color="blue" progress={isProgress2} />
  
    
            <div>
              <img src={url2 ? url2 : newUrl2} className="imgPost"></img>
            </div>
<Button onClick={addSecondProfilePicture } fill color="green" className="placeAddFriendCol">Add To Profile </Button>
</Block>
</Col>
</Row>






<Col> <Button href='/completeAccount/' fill color="green" className="placeAddProfileCol" disabled={disable}>
           Complete account
            </Button></Col>
      </Tab>
    </Tabs>
          </Page>
        </View>
      </Popup>


  {/* Popup 
  events

  */}
  <Popup id="my-EventsPopup">
        <View>
          <Page>
            <Navbar title="Events">
              <NavRight>
                <Link popupClose>Close</Link>
              </NavRight>
            </Navbar>
            <BlockTitle>Events</BlockTitle>
            <Block strong>
        <Row>
          <Col className="text-align-center">
            <Gauge
              type="circle"
              value={0.44}
              valueText="44 likes"
              valueTextColor="#ff9800"
              borderColor="#ff9800"
            />
          </Col>
          <Col className="text-align-center">
            <Gauge
              type="circle"
              value={0.5}
              valueText="120"
              valueTextColor="#4caf50"
              borderColor="#4caf50"
              labelText="Total friends"
              labelTextColor="#f44336"
              labelFontWeight={700}
            />
          </Col>
          
        </Row>
      </Block>

          </Page>
        </View>
      </Popup>



  {/* Popup 
  
  rapport user
  
  */}
  <Popup id="my-RaportPopup">
        <View>
          <Page>
            <Navbar title="Raport user">
              <NavRight>
                <Link popupClose>Close</Link>
              </NavRight>
            </Navbar>
            <BlockTitle>Did you see something wrong? Raport Here !</BlockTitle>

            <List>
              
            <ListInput
            outline
    label="name of user you want to raport."
    floatingLabel
    type="textarea"
    resizable
    value={nameOfUserGetsRaported}
    placeholder="Enter here."
    onChange={(event) => {setNameOfUserGetsRaported(event.target.value)}}
    >
  
  
  </ListInput>

  <BlockTitle >Reason Why?</BlockTitle>
          <Input
            outline
  value={textOfUserRaporter}
    floatingLabel
    type="text"
    resizable
className="raportInputText"
    placeholder="Describe it in your own words"
    onChange={(event) => {  setTextOfUserRaporter(event.target.value)}}
    ></Input>
  </List>
  <List>
  

  <ListItem title="What did the user wrong?" smartSelect smartSelectParams={{ openIn: 'sheet' }} >
        <select name="chooseReason"  onChange={(event) => {setReasonWhy(event.target.value)}}>
        <option value="geen reden gegeven">Geef een reden</option>
          <option value="weird">raar gedrag van user</option>
          <option value="trolling">pest andere mensen</option>
          <option value="wrong_content">is bezig met het delen van vreemde content</option>
          <option value="illegal">doet illegale dingen</option>
          
        </select>
      </ListItem>
  </List>
  <Button fill color="red" className="placePostCol" onClick={() => raportUser()}>
              Raport user
            </Button>

          </Page>
        </View>
      </Popup>



  
 
   {/* Popup 
 friend requests
  */}
  <Popup id="my-FriendshipRequestPopup">
        <View>
          <Page>
            <Navbar title="Friendship Requests">
              <NavRight>
                <Link popupClose>Close</Link>
              </NavRight>
            </Navbar>
            <BlockTitle>Open Friendship Requests</BlockTitle>
          
            <List mediaList>
            {friendshipRequests.map((request) => { return <ListItem key={request.id} title={request.userName}>
        <img
          slot="media"
          src={request.userPhotoUrl}
          width="60"
          height="63"
        />
        <Row style={{ 'paddingTop': '5px' }}>
          <Col>
            <Button onClick={() =>PermitFriendshipRequest(request.id)}  fill color="green" >
            Permit
            </Button>
          </Col>
          <Col>
            <Button onClick={() =>DenyFriendshipRequest(request.id)} fill color="red">
           Deny
            </Button>
          </Col>
        </Row>
      </ListItem>
       })}
    </List>

          </Page>
        </View>
      </Popup>


  


    </App>
    </AuthProvider>
  )

}
export default MyApp;