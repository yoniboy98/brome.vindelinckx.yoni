import React, {useRef, useState, useContext} from 'react';
import {
    Page,
    View,
    Navbar,
    NavRight,
    BlockTitle,
    List,
    ListInput,
    Progressbar,
    ListItem,
    TextEditor,
    Icon,
    Button,
    f7,
    Input,
    Link

} from 'framework7-react';
 import { initializeApp } from 'firebase/app';
import { getFirestore,addDoc, collection, updateDoc, doc, setDoc} from 'firebase/firestore';
import { getAuth, signInWithPopup, GoogleAuthProvider, signOut } from "firebase/auth";
import { getStorage, ref ,uploadBytesResumable, getDownloadURL, uploadString, uploadBytes} from "firebase/storage";
import { AuthContext } from '../context/Auth';


const firebaseConfig = {
    apiKey: "AIzaSyAi2IBMGJ_En3_hIRha4wwAVkSu7nPVraY",
  authDomain: "brome-5e406.firebaseapp.com",
  databaseURL: "https://brome-5e406-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "brome-5e406",
  storageBucket: "brome-5e406.appspot.com",
  messagingSenderId: "130755062271",
  appId: "1:130755062271:web:afdd4c7264793564de5fca",
  measurementId: "G-ZBXLWKV8L4"
  };
  
  
  const db = getFirestore(app);
  const app = initializeApp(firebaseConfig);
  const storage = getStorage(app);


  const CompleteAccount = () => {


      /* user data */


  const [about, setAbout] = useState("");

  const [phonenumber, setPhonenumber] = useState("");

  const [work, setWork] = useState("");

  const [gender, setGender] = useState("");

  const [birth, setBirth] = useState("");

  const [living, setLiving] = useState("");


  const {newCurrentUser} = useContext(AuthContext)


  const SendNewUserData =  async (userId) => {
  
    setDoc(doc(db, "users", userId), {
      userId: newCurrentUser.uid,
      userName: newCurrentUser.displayName,
      userEmail: newCurrentUser.email,
      photoUrl: newCurrentUser.photoURL,
      isOnline: true,
     userAbout: about,
    userPhoneNumber: phonenumber,
    userWork: work,
    userGender: gender,
    userBirth: birth,
    userLiving: living,
    updatedAt: new Date()
    })
    .then(() => {
       f7.dialog.alert('de gegevens, zijn toegevoegd!');

    //alle inputfields leegmaken
      //met clearButton dat bij elke ListInput staat
    })
    .catch(() => {
      f7.dialog.alert('de gegevens toevoegen is niet gelukt, probeer opnieuw');
      return;
    });
  };



    return( <Page>
        <Navbar title="Complete Information">
          <NavRight>
          <Link back>Close</Link>
          </NavRight>
        </Navbar>
      
        <List noHairlinesMd>
  <ListInput
    outline
    label="About you"
    floatingLabel
    type="text"
    placeholder="Tell something about yourself !"
    clearButton
    onChange={(event) => {setAbout(event.target.value)}}
  >
    <Icon icon="demo-list-icon" slot="media" />
  </ListInput>
  <ListInput
    outline
    label="Where do you live?"
    floatingLabel
    type="text"
    placeholder="Place of living"
    clearButton
    onChange={(event) => {setLiving(event.target.value)}}
  >
    <Icon icon="demo-list-icon" slot="media" />
  </ListInput>
  <ListInput
    outline
    label="What is your day of birth?"
    floatingLabel
    type="date"
    placeholder="Date of birth"
    clearButton
    onChange={(event) => {setBirth(event.target.value)}}
  >
    <Icon icon="demo-list-icon" slot="media" />
  </ListInput>
  <ListInput
    outline
    label="What is your gender?"
    floatingLabel
    type="text"
    placeholder="Gender"
    clearButton
    onChange={(event) => {setGender(event.target.value)}}
  >
    <Icon icon="demo-list-icon" slot="media" />
  </ListInput>
  <ListInput
    outline
    label="Phone"
    floatingLabel
    type="tel"
    placeholder="Your phone number"
    clearButton
    onChange={(event) => {setPhonenumber(event.target.value)}}
  >
    <Icon icon="demo-list-icon" slot="media" />
  </ListInput>
  <ListInput
    outline
    label="What do you for hobby's?"
    floatingLabel
    type="textarea"
    resizable
    placeholder="Bio"
    clearButton
    onChange={(event) => {setWork(event.target.value)}}
  >
    <Icon icon="demo-list-icon" slot="media" />
  </ListInput>

  <Button onClick={() => SendNewUserData(newCurrentUser.uid)} fill>Add data to your account</Button>
</List>
    

        </Page>


    )}


  export default CompleteAccount
  