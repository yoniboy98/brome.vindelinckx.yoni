import HomePage from '../pages/home.jsx';
import FriendPage from '../pages/friends.jsx';
import SettingsPage from '../pages/settings.jsx';
import UserMessagesPage from '../pages/userMessages.jsx';
import DynamicRoutePage from '../pages/dynamic-route.jsx';
import RequestAndLoad from '../pages/request-and-load.jsx';
import NotFoundPage from '../pages/404.jsx';
import Post from '../components/posts.jsx';
import CompleteAccount from '../components/CompleteAccount.jsx';



/*function checkAuth({ to, from, resolve, reject }) {
  if (1>0) {
    console.log('resolve')
    resolve();
  } else {
    console.log('reject')
    reject();
  }
}*/
var routes = [
  {
    path: '/',
    component: HomePage,
   
  },
  {
    path: '/post/',
    component: Post,
    
      },
      {
        path: '/completeAccount/',
        component: CompleteAccount,
        
          },
         
     
  {
    path: '/friends/',
    component: FriendPage,
  //  beforeEnter: [checkAuth]
  },

  {
    path: '/settings/',
    component: SettingsPage,
    
  },
  {
    path: '/userMessages/:id/',
    component: UserMessagesPage,

},
  {
    path: '/dynamic-route/blog/:blogId/post/:postId/',
    component: DynamicRoutePage,
  },
  {
    path: '/request-and-load/user/:userId/',
    async: function ({ router, to, resolve }) {
      // App instance
      var app = router.app;
  

      // Show Preloader
      app.preloader.show();

      // User ID from request
      var userId = to.params.userId;

      // Simulate Ajax Request
      setTimeout(function () {
        // We got user data from request
        var user = {
          firstName: 'Vladimir',
          lastName: 'Kharlampidi',
          about: 'Hello, i am creator of Framework7! Hope you like it!',
          links: [
            {
              title: 'Framework7 Website',
              url: 'http://framework7.io',
            },
            {
              title: 'Framework7 Forum',
              url: 'http://forum.framework7.io',
            },
          ]
        };
        // Hide Preloader
        app.preloader.hide();

        // Resolve route to load page
        resolve(
          {
            component: RequestAndLoad,
          },
          {
            props: {
              user: user,
            }
          }
        );
      }, 1000);
    },
  },
  {
    path: '(.*)',
    component: NotFoundPage,
  },
];

export default routes;
